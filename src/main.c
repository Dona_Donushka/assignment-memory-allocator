#include "tests.h"

int main()
{
    void* heap_ptr = before_tests();

    // пустая куча
    empty_heap_test(heap_ptr);

    // выделить  блок
    malloc_test(heap_ptr);

    // освободить
    free_test(heap_ptr);

    free_with_merge_test(heap_ptr);

    //region_extends_test(heap_ptr);

    region_extends_without_merge_test(heap_ptr);

    return 0;
}
