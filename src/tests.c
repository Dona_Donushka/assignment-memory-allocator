#define _DEFAULT_SOURCE
#include "tests.h"
#include "mem_internals.h"

#include <unistd.h>

void* before_tests() {
    return heap_init(REGION_MIN_SIZE);
}

bool empty_heap_test(const void* heap_ptr) {
    const char* test_name = "Empty heap";

//  EXPECTED ---------------------------
    struct mem_state expected;

    const size_t EXPECTED_BLOCKS_NUMBER = 1;
    uint8_t* EXPECTED_NEXT_ADDRESS = NULL;
    const size_t EXPECTED_FREE_CAPACITY = REGION_MIN_SIZE - struct_block_header_size() * EXPECTED_BLOCKS_NUMBER;

    expected.length = EXPECTED_BLOCKS_NUMBER;
    struct block_header block1 = {
        .capacity = { EXPECTED_FREE_CAPACITY },
        .is_free = true,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    expected.blocks[0] =  &block1;

// ACTUAL ---------------------------------
    struct mem_state actual = get_mem_state(heap_ptr);

// ASSERTION ------------------------------
    bool result = assert_memory_state(&expected, &actual);

// PRINT RESULTS --------------------------
    print_test_result(test_name, result, &expected, &actual);

    return result;
}
//------------------------------------------------------------------------------
bool malloc_test(const void* heap_ptr) {
    const char* test_name = "Function _malloc()";

//  EXPECTED ---------------------------
    const size_t EXPECTED_BLOCKS_NUMBER = 2;
    const size_t EXPECTED_BLOCK_CAPACITY = 0x100;
    uint8_t* EXPECTED_NEXT_ADDRESS = (uint8_t*)heap_ptr + EXPECTED_BLOCK_CAPACITY + struct_block_header_size();
    const size_t EXPECTED_FREE_CAPACITY = REGION_MIN_SIZE - EXPECTED_BLOCK_CAPACITY - struct_block_header_size() * EXPECTED_BLOCKS_NUMBER;

    struct mem_state expected;
    expected.length = EXPECTED_BLOCKS_NUMBER;

    // block 1 -----------
    struct block_header block1 = {
        .capacity = { EXPECTED_BLOCK_CAPACITY },
        .is_free = false,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };
    expected.blocks[0] =  &block1;

    // last block --------
    EXPECTED_NEXT_ADDRESS = NULL;

    struct block_header last_block = {
        .capacity = { EXPECTED_FREE_CAPACITY },
        .is_free = true,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };
    expected.blocks[1] =  &last_block;

// ACTUAL ---------------------------------
    void* p = _malloc(EXPECTED_BLOCK_CAPACITY);
    struct mem_state actual = get_mem_state(heap_ptr);

// ASSERTION ------------------------------
    bool result = assert_memory_state(&expected, &actual);

// PRINT RESULTS --------------------------
    print_test_result(test_name, result, &expected, &actual);

    _free(p);

    return result;
}

bool free_test(const void* heap_ptr) {
    const char* test_name = "Function _free() without blocks merge";

//  EXPECTED ---------------------------
    struct mem_state expected;

    const size_t EXPECTED_BLOCKS_NUMBER = 4;
    const size_t EXPECTED_BLOCK_CAPACITY = 0x100;

    // block1 ------
    uint8_t* EXPECTED_NEXT_ADDRESS = (uint8_t*)heap_ptr + EXPECTED_BLOCK_CAPACITY + struct_block_header_size();
    struct block_header block1 = {
        .capacity = { EXPECTED_BLOCK_CAPACITY },
        .is_free = false,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    // block2 --------------
    EXPECTED_NEXT_ADDRESS += EXPECTED_BLOCK_CAPACITY + struct_block_header_size();
    struct block_header block2 = {
        .capacity = { EXPECTED_BLOCK_CAPACITY },
        .is_free = true,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    // block3 --------------
    EXPECTED_NEXT_ADDRESS += EXPECTED_BLOCK_CAPACITY + struct_block_header_size();
    struct block_header block3 = {
        .capacity = { EXPECTED_BLOCK_CAPACITY },
        .is_free = false,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    // last block  --------------
    EXPECTED_NEXT_ADDRESS = NULL;
    const size_t EXPECTED_FREE_CAPACITY = REGION_MIN_SIZE
        - EXPECTED_BLOCK_CAPACITY * (EXPECTED_BLOCKS_NUMBER - 1)
        - struct_block_header_size() * EXPECTED_BLOCKS_NUMBER;

    struct block_header block4 = {
        .capacity = { EXPECTED_FREE_CAPACITY },
        .is_free = true,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    expected.length = EXPECTED_BLOCKS_NUMBER;
    expected.blocks[0] =  &block1;
    expected.blocks[1] =  &block2;
    expected.blocks[2] =  &block3;
    expected.blocks[3] =  &block4;

// ACTUAL ---------------------------------
    void* ptr1 = _malloc(0x100);
    void* ptr2 = _malloc(0x100);
    void* ptr3 = _malloc(0x100);
    _free(ptr2);

    struct mem_state actual = get_mem_state(heap_ptr);

// ASSERTION ------------------------------
    bool result = assert_memory_state(&expected, &actual);

// PRINT RESULTS --------------------------
    print_test_result(test_name, result, &expected, &actual);

    _free(ptr3);
    _free(ptr1);
    return result;
}

bool free_with_merge_test(const void* heap_ptr) {
    const char* test_name = "Function _free() with blocks merge";

//  EXPECTED ---------------------------
    struct mem_state expected;

    const size_t EXPECTED_BLOCKS_NUMBER = 3;
    const size_t EXPECTED_BLOCK_CAPACITY = 0x100;

    // block1 ------
    uint8_t* EXPECTED_NEXT_ADDRESS = (uint8_t*)heap_ptr + (EXPECTED_BLOCK_CAPACITY + struct_block_header_size()) * (EXPECTED_BLOCKS_NUMBER - 1);
    struct block_header block1 = {
        .capacity = { EXPECTED_BLOCK_CAPACITY * (EXPECTED_BLOCKS_NUMBER - 1) + struct_block_header_size() },
        .is_free = true,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    // block2 --------------
    EXPECTED_NEXT_ADDRESS += EXPECTED_BLOCK_CAPACITY + struct_block_header_size();
    struct block_header block2 = {
        .capacity = { EXPECTED_BLOCK_CAPACITY },
        .is_free = false,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    // last block  --------------
    EXPECTED_NEXT_ADDRESS = NULL;
    const size_t EXPECTED_FREE_CAPACITY = REGION_MIN_SIZE
        - EXPECTED_BLOCK_CAPACITY * (EXPECTED_BLOCKS_NUMBER)
        - struct_block_header_size() * (EXPECTED_BLOCKS_NUMBER + 1);

    struct block_header block3 = {
        .capacity = { EXPECTED_FREE_CAPACITY },
        .is_free = true,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };


    expected.length = EXPECTED_BLOCKS_NUMBER;
    expected.blocks[0] =  &block1;
    expected.blocks[1] =  &block2;
    expected.blocks[2] =  &block3;

// ACTUAL ---------------------------------
    void* ptr1 = _malloc(0x100);
    void* ptr2 = _malloc(0x100);
    void* ptr3 = _malloc(0x100);
    _free(ptr2);
    _free(ptr1);

    struct mem_state actual = get_mem_state(heap_ptr);

// ASSERTION ------------------------------
    bool result = assert_memory_state(&expected, &actual);

// PRINT RESULTS --------------------------
    print_test_result(test_name, result, &expected, &actual);

    _free(ptr3);
    return result;
}

bool region_extends_test(const void* heap_ptr) {
    const char* test_name = "Extends region with merge";

//  EXPECTED ---------------------------
    struct mem_state expected;

    const size_t EXPECTED_BLOCKS_NUMBER = 3;
    const size_t EXPECTED_BLOCK_CAPACITY = 0x100;
// Block1 --------------------
    uint8_t* EXPECTED_NEXT_ADDRESS = (uint8_t*)heap_ptr + REGION_MIN_SIZE;
    struct block_header block1 = {
        .capacity = { REGION_MIN_SIZE - struct_block_header_size() },
        .is_free = false,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

// Block2 --------------------
    EXPECTED_NEXT_ADDRESS += EXPECTED_BLOCK_CAPACITY + struct_block_header_size();
    struct block_header block2 = {
        .capacity = { EXPECTED_BLOCK_CAPACITY },
        .is_free = false,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };
// Last Block ----------------
    EXPECTED_NEXT_ADDRESS = NULL;
    struct block_header block3 = {
        .capacity = { REGION_MIN_SIZE - EXPECTED_BLOCK_CAPACITY - struct_block_header_size() * (EXPECTED_BLOCKS_NUMBER - 1) },
        .is_free = true,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    expected.length = EXPECTED_BLOCKS_NUMBER;
    expected.blocks[0] =  &block1;
    expected.blocks[1] =  &block2;
    expected.blocks[2] =  &block3;

// ACTUAL ---------------------------------
    void* ptr1 = _malloc(REGION_MIN_SIZE - struct_block_header_size());
    void* ptr2 = _malloc(EXPECTED_BLOCK_CAPACITY);

    struct mem_state actual = get_mem_state(heap_ptr);

// ASSERTION ------------------------------
    bool result = assert_memory_state(&expected, &actual);

// PRINT RESULTS --------------------------
    print_test_result(test_name, result, &expected, &actual);

    _free(ptr2);
    _free(ptr1);
    return result;

}

bool region_extends_without_merge_test(const void* heap_ptr){
    const char* test_name = "Extends region without regions merge";

//  EXPECTED ---------------------------
    struct mem_state expected;

    const size_t EXPECTED_BLOCKS_NUMBER = 3;
    const size_t EXPECTED_BLOCK_CAPACITY = 0x100;
// Block1 --------------------
    uint8_t* EXPECTED_NEXT_ADDRESS = NULL;
    struct block_header block1 = {
        .capacity = { REGION_MIN_SIZE - struct_block_header_size() },
        .is_free = false,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

// Block2 --------------------
    struct block_header block2 = {
        .capacity = { EXPECTED_BLOCK_CAPACITY },
        .is_free = false,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };
// Last Block ----------------
    struct block_header block3 = {
        .capacity = { REGION_MIN_SIZE - EXPECTED_BLOCK_CAPACITY - struct_block_header_size() * (EXPECTED_BLOCKS_NUMBER - 1) },
        .is_free = true,
        .next = (struct block_header*)EXPECTED_NEXT_ADDRESS
    };

    expected.length = EXPECTED_BLOCKS_NUMBER;
    expected.blocks[0] =  &block1;
    expected.blocks[1] =  &block2;
    expected.blocks[2] =  &block3;

// ACTUAL ---------------------------------
    // try to prevent regions merging
    mmap(
        (uint8_t*)heap_ptr+REGION_MIN_SIZE,
        getpagesize(),
        PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,
        0, 0 );
    //--------------------------------------
    void* ptr1 = _malloc(REGION_MIN_SIZE - struct_block_header_size());
    void* ptr2 = _malloc(EXPECTED_BLOCK_CAPACITY);

    struct mem_state actual = get_mem_state(heap_ptr);

// ASSERTION ------------------------------
    bool result = assert_memory_state_without_next(&expected, &actual);

// PRINT RESULTS --------------------------
    print_test_result(test_name, result, &expected, &actual);

    _free(ptr2);
    _free(ptr1);
    return result;

}
