#include "testutil.h"
#include "mem_internals.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct mem_state get_mem_state(const void* const ptr) {
    struct mem_state state;
    state.length = 0;
    for(struct block_header const* header =  ptr; header && state.length < TEST_BLOCKS_SIZE; header = header ->next ) {
        state.blocks[state.length] = header;
        state.length++;
    }
    return state;
}

void print_mem_state(const struct mem_state* state) {
    printf("Blocks count: %zu\n", state->length);
    printf("%-10s %-8s %-10s\n", "Capacity", "Status", "Next" );
    for(size_t i = 0; i < state->length; i++) {
        printf("%-10zu %-8s %-10p\n",
               state->blocks[i]->capacity.bytes,
               state->blocks[i]->is_free? "free" : "taken",
               (void*)state->blocks[i]->next
               );
    }
}
////////////////////////////////////////////////////////
size_t struct_block_header_size() {
    return offsetof( struct block_header, contents );
}
/////////////////////////////////////////////////////////
bool assert_memory_state(const struct mem_state* expected, const struct mem_state* actual) {
    if (expected->length != actual->length) return false;

    for(size_t i = 0; i < expected->length; i++) {
        if (
            actual->blocks[i]->capacity.bytes != expected->blocks[i]->capacity.bytes
            || actual->blocks[i]->is_free != expected->blocks[i]->is_free
            || actual->blocks[i]->next != expected->blocks[i]->next
        ) {
            return false;
        }
    }
    return true;
}

bool assert_memory_state_without_next(const struct mem_state* expected, const struct mem_state* actual) {
    if (expected->length != actual->length) return false;

    for(size_t i = 0; i < expected->length; i++) {
        if (
            actual->blocks[i]->capacity.bytes != expected->blocks[i]->capacity.bytes
            || actual->blocks[i]->is_free != expected->blocks[i]->is_free
        ) {
            return false;
        }
    }
    return true;
}
/////////////////
void print_test_result(const char* test_name, bool result, const struct mem_state* expected, const struct mem_state* actual) {
    printf("TEST: \033[33m%s\033[0m\n", test_name);
    printf("Status: %s\n\n", result ? "\033[32mSuccess\033[0m" : "\033[31mFail\033[0m");

    printf("---- Expected ----\n");
    print_mem_state(expected);
    printf("\n");

    printf("---- Actual ----\n");
    print_mem_state(actual);
    printf("\n-----------------------------\n");
}
