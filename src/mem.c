#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  /* IMPLEMENTATION */
    if(!addr) return REGION_INVALID;

    struct region new_region = REGION_INVALID;
    new_region.size = region_actual_size(query);

    int additional_flags =  MAP_FIXED_NOREPLACE;

    new_region.addr = map_pages(addr, new_region.size, additional_flags);

    if (new_region.addr == MAP_FAILED && addr != HEAP_START) {
        new_region.addr = map_pages(NULL, new_region.size, 0);
    }

    if (new_region.addr == MAP_FAILED) return REGION_INVALID;


    new_region.extends = new_region.addr == addr;

    block_init(new_region.addr, (block_size){new_region.size}, NULL);

    return new_region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/* NEW FUNCTION */
static block_capacity get_block_capacity ( size_t query ) {
    return query > BLOCK_MIN_CAPACITY
        ? (block_capacity) { query }
        : (block_capacity) { BLOCK_MIN_CAPACITY };
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    /* IMPLEMENTATION */
    if (!block_splittable(block, query)) return false;

    block_size old_size = size_from_capacity( block->capacity );
    struct block_header* old_next_block = block->next;

    block_size updated_size = size_from_capacity( get_block_capacity(query) );
    block_size new_block_size = { old_size.bytes - updated_size.bytes };
    // calculate position of new block
    struct block_header* new_block = (struct block_header*) ( (uint8_t*)block + updated_size.bytes );
    // calculate size of new block
    // create new block
    block_init(new_block, new_block_size, old_next_block);

    // update current block
    block_init(block, updated_size, new_block);

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    /* IMPLEMENTATION */
    if(!block->next || !mergeable(block, block->next)) return false;

    block_size merged_block_size = {
        size_from_capacity(block->capacity).bytes
        + size_from_capacity(block->next->capacity).bytes
    };

    block_init(block, merged_block_size, block->next->next);
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t query )    {
    /* IMPLEMENTATION */
    if (!block) return (struct block_search_result) {BSR_CORRUPTED, NULL};

    for (struct block_header* b = block; b; b = b->next) {
        while(try_merge_with_next(b));

        if (b->is_free && block_is_big_enough(query, b)) {
            return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, b};
        }

        if (!b->next) {
            return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, b};
        }
    }

    return (struct block_search_result) { BSR_CORRUPTED, NULL};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    /* IMPLEMENTATION */
    struct block_search_result result = find_good_or_last(block, query);
    switch (result.type) {
        case BSR_FOUND_GOOD_BLOCK:
            split_if_too_big(result.block, query);
            break;
        case BSR_REACHED_END_NOT_FOUND:
            break;
        case BSR_CORRUPTED:
            break;
    }
    return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  /*  IMPLEMENTATION */
    if(!last) return NULL;

    void* next_block_addr = block_after(last);

    size_t new_region_size = query + offsetof( struct block_header, contents );
    struct region new_region = alloc_region( next_block_addr, new_region_size ); //size
    if (region_is_invalid(&new_region)) return NULL;

    last->next = new_region.addr;
    if (new_region.extends) {
        try_merge_with_next(last);
    }
    return last;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    /*  IMPLEMENTATION */
    if (!heap_start) return NULL;

    struct block_search_result result = try_memalloc_existing(query, heap_start);

    if (result.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header* last = grow_heap(result.block, query);
        if (!last) return NULL;

        result = try_memalloc_existing(query, last);
    }

    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        result.block->is_free = false;
        return result.block;
    }

    return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  /*  IMPLEMENTATION */
    while(try_merge_with_next(header));

}
