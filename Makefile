CFLAGS=--std=c17 -Wall -pedantic -Iinclude/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
SRCDIR=src
INCDIR=include
CC=gcc

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/testutil.o $(BUILDDIR)/tests.o $(BUILDDIR)/main.o
	$(CC) -o $(BUILDDIR)/memory-allocator $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/mem.o: $(SRCDIR)/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@
	
$(BUILDDIR)/testutil.o: $(SRCDIR)/testutil.c build
	$(CC) -c $(CFLAGS) $< -o $@	

$(BUILDDIR)/tests.o: $(SRCDIR)/tests.c build
	$(CC) -c $(CFLAGS) $< -o $@
		
$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
