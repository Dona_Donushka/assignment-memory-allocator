#ifndef _TESTUTIL_H_
#define _TESTUTIL_H_

#include "mem.h"

#define TEST_BLOCKS_SIZE 20

struct mem_state {
    const struct block_header* blocks[TEST_BLOCKS_SIZE];
    size_t length;
};

struct mem_state get_mem_state(const void* const ptr);
void print_mem_state(const struct mem_state* state);
size_t struct_block_header_size();
bool assert_memory_state(const struct mem_state* expected, const struct mem_state* actual);
bool assert_memory_state_without_next(const struct mem_state* expected, const struct mem_state* actual);
void print_test_result(const char* test_name, bool result, const struct mem_state* expected, const struct mem_state* actual);

#endif
