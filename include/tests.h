#ifndef _TESTS_H_
#define _TESTS_H_

#include "testutil.h"

void* before_tests();
bool empty_heap_test(const void* heap_ptr);
bool malloc_test(const void* heap_ptr);
bool free_test(const void* heap_ptr);
bool free_with_merge_test(const void* heap_ptr);
bool region_extends_test(const void* heap_ptr);
bool region_extends_without_merge_test(const void* heap_ptr);

#endif
